DROP SCHEMA IF EXISTS F1_Championship;
CREATE SCHEMA F1_Championship DEFAULT CHARACTER SET utf8;
USE F1_Championship;

CREATE TABLE timovi (
	id INT AUTO_INCREMENT, 
	naziv VARCHAR(50) NOT NULL, 
	sediste VARCHAR(50) NOT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE vozaci (
	id BIGINT AUTO_INCREMENT, 
    timId INT NOT NULL, 
	imeIPrezime VARCHAR(50) NOT NULL, 
    poeni INT NOT NULL DEFAULT 0, 
    datumRodjenja DATE NOT NULL,
    brojVozaca INT NOT NULL UNIQUE DEFAULT 0, 
    pozicijaVozaca ENUM('Primarni', 'Sekundarni' , 'Rezerva') DEFAULT 'Primarni',
	PRIMARY KEY(id), 
	FOREIGN KEY(timId) REFERENCES timovi(id)
		ON DELETE RESTRICT,
	CONSTRAINT uc_tim_pozicijaVozaca UNIQUE(timId,pozicijaVozaca)
);

CREATE TABLE korisnici (
	korisnickoIme VARCHAR(20), 
	lozinka VARCHAR(20) NOT NULL, 
	eMail VARCHAR(20) NOT NULL, 
	pol ENUM('muški', 'ženski') DEFAULT 'muški', 
	administrator BOOL DEFAULT false, 
	PRIMARY KEY(korisnickoIme)
);

INSERT INTO timovi (id, naziv, sediste) VALUES (1, 'Mercedes', 'Brackley, United Kingdom');
INSERT INTO timovi (id, naziv, sediste) VALUES (2, 'Red Bull Racing', 'Milton Keynes, United Kingdom');
INSERT INTO timovi (id, naziv, sediste) VALUES (3, 'Renault', 'Enstone, United Kingdom');
INSERT INTO timovi (id, naziv, sediste) VALUES (4, 'McLaren', 'Woking, United Kingdom');
INSERT INTO timovi (id, naziv, sediste) VALUES (5, 'Racing Point', 'Silverstone, United Kingdom');
INSERT INTO timovi (id, naziv, sediste) VALUES (6, 'Ferrari', 'Maranello, Italy');
INSERT INTO timovi (id, naziv, sediste) VALUES (7, 'AlphaTauri', 'Faenza, Italy');
INSERT INTO timovi (id, naziv, sediste) VALUES (8, 'Alfa Romeo', 'Hinwil, Switzerland');
INSERT INTO timovi (id, naziv, sediste) VALUES (9, 'Haas F1 Team', 'Kannapolis, United States');
INSERT INTO timovi (id, naziv, sediste) VALUES (10, 'Williams', 'Grove, United Kingdom');

INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) 
	VALUES (1, 'Lewis Hamilton', 3713, '1985-01-07', 44, 'Primarni');
INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) 
	VALUES (1, 'Valtteri Botta', 3713, '1989-08-28', 77, 'Sekundarni');
INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) 
	VALUES (2, 'Max Verstappen', 1110, '1997-09-30', 33, 'Primarni');
INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) 
	VALUES (2, 'Alexander Albon', 156, '1996-03-23', 23, 'Sekundarni');
INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) 
	VALUES (3, 'Daniel Ricciardo', 1135, '1989-07-01', 3, 'Primarni');

INSERT INTO korisnici (korisnickoIme, lozinka, eMail, pol, administrator) VALUES ('a', 'a', 'a@a.com', 'muški', true);
INSERT INTO korisnici (korisnickoIme, lozinka, eMail, pol, administrator) VALUES ('b', 'b', 'b@b.com', 'ženski', false);
INSERT INTO korisnici (korisnickoIme, lozinka, eMail, pol, administrator) VALUES ('c', 'c', 'c@c.com', 'muški', false);
