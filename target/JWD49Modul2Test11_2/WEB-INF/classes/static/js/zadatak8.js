function validacija() {
	var greska = false
	if (document.getElementById("naziv").value == "") {
		greska = true;
		alert("Popunite polje naziv tima.")
	}
	if (document.getElementById("imeIPrezime").value == "") {
		greska = true;
		alert("Popunite polje ime i prezime.")
	}
	if (document.getElementById("poeni").value == "") {
		greska = true;
		alert("Popunite polje poeni.")
	}
	if (document.getElementById("datumRodjenja").value == "" || isDate(document.getElementById("datumRodjenja"))) {
		greska = true;
		alert("Popunite polje datum rodjenja.")
	}
	if (document.getElementById("brojVozaca").value == "" && document.getElementById("brojVozaca") > "100") {
		greska = true;
		alert("Broj vozaca mora biti od 1 do 100.")
	}
	if (document.getElementById("pozicijaVozaca").value == "") {
		greska = true;
		alert("Popunite polje pozicija vozaca.")
	}
	if (greska === true) {
		event.preventDefault()

	}

}