var baseURL = ""

function popuniBaseURL() {
	// traži od servera baseURL
	$.get("Timovi/baseURL", function(odgovor) { // GET zahtev
		console.log(odgovor)

		if (odgovor.status == "ok") {
			baseURL = odgovor.baseURL // inicjalizuj globalnu promenljivu baseURL
			$("base").attr("href", baseURL) // postavi href atribut base elementa
		}
	})
	console.log("GET: " + "baseURL")
}


function popuniTimove() {
	var tabela = $("table.tabela")
	var nazivInput = $("input[name=naziv]")
	var sedisteInput = $("input[name=sediste]")
	var naziv = nazivInput.val()
	var sediste = sedisteInput.val()

	var params = {
		naziv: naziv,
		sediste: sediste
	}
	console.log(params)
	$.get("Timovi", params, function(odgovor) {
		console.log(odgovor)

		if (odgovor.status == "ok") {
			tabela.find("tr:gt(1)").remove()

			var timovi = odgovor.timovi
			for (var it in timovi) {
				var gradIDrzava = timovi[it].sediste
				var strx = gradIDrzava.split(',')
				tabela.append(

					'<tr>' +
					'<td>' + timovi[it].naziv + '</td>'+
					'<td>' + strx[0] + '</td>' +
					'<td>' + strx[1] + '</td>' +
					'</tr>'
				)
			}
		}
	})
	console.log("GET: Timovi")
}


function dodajTim() {

	// kupimo parametre iz forme u varijable / promenljive
	var nazivInput = $("input[name=naziv]")
	var sedisteInput = $("input[name=sediste]")
	var naziv = nazivInput.val()
	var sediste = sedisteInput.val()
	// pravimo JSON objekat od pokupljenih podataka
	var params = {
		naziv: naziv,
		sediste: sediste
	}
	console.log(params)

	// pravimo / ispucavate POST request ka serveru ( odgovarajucem kontroleru)

	$.post("Timovi/Create", params, function(odgovor) {

		// telo od funkcije - function(odgovor) - prestavlja funkciju koja se izvrsi tek kad dobijemo odgovor od servera
		// ovde racunaj cenu za zadatak7.html /Parking
		console.log(odgovor)

		if (odgovor.status == "ok") {
			window.location.replace("zadatak9.html")
			// } else if (odgovor.status == "neovlascen") {
			// window.location.replace("zanrovi.html")
		} else if (odgovor.status == "greska") {
			$("p.greska").text(odgovor.poruka)
		}
	})
	console.log("POST: Timovi/Create")
}

$(document).ready(function() {
	popuniBaseURL()
	popuniTimove()
	$("form").submit(function() {
		dodajTim()
		return false

	})
})