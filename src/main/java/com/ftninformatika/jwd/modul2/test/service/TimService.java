package com.ftninformatika.jwd.modul2.test.service;

import java.util.List;

import com.ftninformatika.jwd.modul2.test.model.Tim;

public interface TimService {

	List<Tim> findAll();
	Tim save(Tim tim);
	Tim findOne(String naziv);
}
