package com.ftninformatika.jwd.modul2.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.dao.TimDAO;
import com.ftninformatika.jwd.modul2.test.model.Tim;
import com.ftninformatika.jwd.modul2.test.service.TimService;

@Service
public class DatabaseTimService implements TimService {

	@Autowired
	TimDAO timDao;
	
	public List<Tim> findAll() {
	
		return timDao.findAll();
	}
	public Tim save(Tim tim) {
		timDao.save(tim);
		return tim;
	}

	public Tim findOne(String naziv) {
		return timDao.findOne(naziv);
	}

}
