package com.ftninformatika.jwd.modul2.test.Controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftninformatika.jwd.modul2.test.model.Tim;
import com.ftninformatika.jwd.modul2.test.service.TimService;

@Controller
@RequestMapping(value="/Timovi")
public class TimController {

	
	@Autowired
	private TimService timService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 
	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	@ResponseBody
	public Map<String, Object>index(
					@RequestParam String naziv,
					@RequestParam (required=false, defaultValue="") String sediste) {
		List<Tim>timovi = timService.findAll();
		Map<String, Object>odgovor=new LinkedHashMap();
		odgovor.put("status", "ok");
		odgovor.put("timovi", timovi);
		for(Tim t :timovi) {
			System.out.println(t.toString());
		}
		return odgovor;
	}
	
	@GetMapping(value="baseURL")
	@ResponseBody
	public Map<String, Object> baseURL() {
		Map<String, Object> odgovor = new LinkedHashMap<String, Object>();
		odgovor.put("status", "ok");
		odgovor.put("baseURL", baseURL);	
		return odgovor;
	}
	
	@PostMapping(value="/Create")
	@ResponseBody
	public Map<String, Object> dodavanjeTima(
			@RequestParam String naziv,
			@RequestParam (required=false, defaultValue="") String sediste) {
	
		if (naziv.equals("")) {
			Map<String, Object> odgovor = new LinkedHashMap<String, Object>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", "Naziv ne sme biti prazan!");
			return odgovor;
		}
		if (sediste.equals("")) {
			Map<String, Object> odgovor = new LinkedHashMap<String, Object>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", "Sediste tima ne sme biti prazno");
			return odgovor;
		}
		
		Tim t = new Tim(naziv, sediste);
		timService.save(t);
		
		Map<String, Object> odgovor = new LinkedHashMap<String, Object>();
		odgovor.put("status", "ok");
		return odgovor;
	}

}
