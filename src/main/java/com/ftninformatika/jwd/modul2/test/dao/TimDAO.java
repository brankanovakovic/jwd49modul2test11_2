package com.ftninformatika.jwd.modul2.test.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul2.test.model.Tim;
@Component
public class TimDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static class TimRowMapper implements RowMapper<Tim> {

		public Tim mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String naziv = rs.getString(index++);
			String sediste = rs.getString(index++);
			
			Tim tim = new Tim(naziv, sediste);
			return tim;
		}

	}
	
	public List<Tim> findAll() {
		String sql = "SELECT naziv, sediste FROM timovi";
		return jdbcTemplate.query(sql, new TimRowMapper());
	}

	public void save(Tim tim) {
		String sql = "INSERT INTO timovi (naziv, sediste) VALUES (?, ?)";
		jdbcTemplate.update(sql, tim.getNaziv(), tim.getSediste());
	
	}

	public Tim findOne(String naziv) {
		try {
			String sql = "SELECT naziv, sediste FROM timovi WHERE naziv = ?";
			return jdbcTemplate.queryForObject(sql, new TimRowMapper(), naziv);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronadjen
			return null;
		}
	}

}
