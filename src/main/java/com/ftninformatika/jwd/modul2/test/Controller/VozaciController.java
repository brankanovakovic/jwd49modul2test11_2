package com.ftninformatika.jwd.modul2.test.Controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftninformatika.jwd.modul2.test.model.PozicijaVozaca;
import com.ftninformatika.jwd.modul2.test.model.Tim;
import com.ftninformatika.jwd.modul2.test.model.Vozac;
import com.ftninformatika.jwd.modul2.test.service.TimService;
import com.ftninformatika.jwd.modul2.test.service.VozacService;

@Controller
//@RequestMapping(value="/")
public class VozaciController implements ServletContextAware {

	private static final String TIMOVI = "timovi";
	private static final String VOZACI= "vozaci";
	private static final String KORISNIK = "korisnik";
	private Map<Integer, Vozac> vozaci= new HashMap<Integer, Vozac>();
	private Map<Integer, Tim> timovi= new HashMap<Integer, Tim>();
	private Map<Integer, Vozac> vozaciKorisnikaIzSesije = new HashMap();
	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");



	private int id=1;

	@Autowired
	private VozacService VozacService;

	@Autowired
	private TimService timService;


	@Autowired
	private ServletContext servletContext;

	private String bURL;

	public void setServletContext(ServletContext serlvetContext) {
		this.servletContext= serlvetContext;

		bURL= serlvetContext.getContextPath()+"/";

	}

	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath();
		Map<Long, Tim> timovi = new HashMap<Long, Tim>();

		timovi.put(1L,new Tim(1L, "Mercedes","Brackley, United Kingdom"));
		timovi.put(2L,new Tim(2L, "Red Bull Racing","Milton Keynes, United Kingdom"));
		timovi.put(3L,new Tim(3L, "Renault", "Enstone, United Kingdom"));
		timovi.put(4L,new Tim(4L, "McLaren", "Woking, United Kingdom"));
		Map<Integer, Vozac> vozaci = new HashMap();
		Map<Integer, Vozac> korisnik = new HashMap();
		vozaci.put(id,new Vozac(id, 3713, 44, timovi.get(1L), "Lewis Hamilton", LocalDate.of(1985,7,1),PozicijaVozaca.Primarni));
		id++;
		vozaci.put(id,new Vozac(id, 1486, 77, timovi.get(1L), "Valtteri Bottas", LocalDate.of(1989,8,28),PozicijaVozaca.Sekundarni));
		id++;
		vozaci.put(id,new Vozac(id, 1110, 33, timovi.get(1L), "Max Verstappen", LocalDate.of(1997,9,30),PozicijaVozaca.Rezerva));
		id++;
		vozaci.put(id,new Vozac(id, 156, 23, timovi.get(2L), "Alexander Albon", LocalDate.of(1996,3,23),PozicijaVozaca.Sekundarni));
		id++;
		vozaci.put(id,new Vozac(id, 1135, 3, timovi.get(3L), "Daniel Ricciardo", LocalDate.of(1989,7,1),PozicijaVozaca.Primarni));
		id++;
		servletContext.setAttribute(VozaciController.TIMOVI, timovi);
		servletContext.setAttribute(VozaciController.VOZACI, vozaci);
		servletContext.setAttribute(VozaciController.KORISNIK, korisnik);

	}

	@GetMapping(value="/Championship/zadatak1.html")
	public String getVozaciRequestForm() {
		return "zadatak1";
	}

	@PostMapping(value="/Create")
	public void CreateVozac(
			@RequestParam int poeni,
			@RequestParam int brojVozaca,
			@RequestParam String imeIPrezime,
			@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)LocalDate datumRodjenja,
			@RequestParam PozicijaVozaca pozicijaVozaca,
			@RequestParam String naziv,
			HttpSession session,
			HttpServletResponse response) throws IOException {
		Map<Long, Tim> timovi = (Map<Long, Tim>) servletContext.getAttribute(VozaciController.TIMOVI);
		Map<Integer, Vozac> vozaci = (Map<Integer, Vozac>) servletContext.getAttribute(VozaciController.VOZACI);
		Map<Integer, Vozac> korisnik = (Map<Integer, Vozac>) servletContext.getAttribute(VozaciController.KORISNIK);
		boolean greska=false;
		Tim tim = null;	
		for(Tim itTim : timovi.values()) {
			if(itTim.getNaziv().equalsIgnoreCase(naziv)) {
				tim = itTim;
			}
		}

		Vozac vozac = new Vozac(id, poeni, brojVozaca, tim, imeIPrezime, datumRodjenja, pozicijaVozaca);
		//int brojac = Collections.frequency(vozaci.values(), naziv);
//		ArrayList<Vozac> valueList = new ArrayList<Vozac>(vozaci.values());
//		int brojac = Collections.frequency(valueList, naziv);
//

		for(Vozac itVozac : vozaci.values()) {
			if(brojVozaca == itVozac.getBrojVozaca()) {
				System.out.println("Broj vozaca mora biti jedinstvena vrednost.");
				greska=true;
			}
			if(pozicijaVozaca.equals(itVozac.getPozicijaVozaca()) && naziv.equals(itVozac.getTim().getNaziv())) {
				System.out.println("Pozicija vozaca u timu vec postoji.");
				System.out.println("Broj vozaca po timu ne sme biti veci od 3");				
				greska=true;
			}
			
		}
//		if(brojac > 3) {
//			System.out.println("Broj vozaca po timu ne sme biti veci od 3");
//			greska=true;
//		}
		if(greska != true) {
			vozaci.put(id, vozac);
			servletContext.setAttribute(VozaciController.VOZACI, vozaci);
			id++;
		}

		//TRAZE ISPIS U KONZOLI ECLIPS-A I TO JE SLEDECE:
		System.out.println("");
		System.out.println("ISPIS U KONZOLI SVIH VOZACA ZADATAK 3");
		System.out.println();
		for(Vozac v : vozaci.values()) {
			System.out.println(v);			
		}
		if(greska==true) {
			response.sendRedirect(bURL + "zadatak1.html");
		}else {
			response.sendRedirect(bURL+"/Zadatak4");
		}
	}

	@GetMapping(value="/Zadatak4")
	public ModelAndView Zadatak4(
			HttpSession session,
			HttpServletResponse response) throws IOException {

		@SuppressWarnings("unchecked")
		Map<Integer, Vozac> vozaci = (Map<Integer, Vozac>) servletContext.getAttribute(VozaciController.VOZACI);
		Map<Integer, Vozac> korisnik = (Map<Integer, Vozac>) servletContext.getAttribute(VozaciController.KORISNIK);
		ModelAndView rezultat = new ModelAndView("zadatak4");
		rezultat.addObject("vozaci", vozaci.values());
		Vozac vozacSaNajmanjimBrojemPoena=vozaci.get(1);
		Vozac vozacSaNajvecimBrojemPoena=vozaci.get(1);

		for(Vozac v : vozaci.values()) {
			if(v.getPoeni() < vozacSaNajmanjimBrojemPoena.getPoeni()) {
				vozacSaNajmanjimBrojemPoena = v;
			}
			if(v.getPoeni() > vozacSaNajvecimBrojemPoena.getPoeni()) {
				vozacSaNajvecimBrojemPoena = v;
			}
		}
		rezultat.addObject("vozacSaNajvecimBrojemPoena", vozacSaNajvecimBrojemPoena);
		rezultat.addObject("vozacSaNajmanjimBrojemPoena", vozacSaNajmanjimBrojemPoena);
		// PRIKAZ ZADATAK4.HTML NE RADI PROVERI ZASTO!!!! :)
		return rezultat;
	}


	@GetMapping(value="/Zadatak7")
	public ModelAndView Zadatak7(
			@RequestParam (required=false, defaultValue = "0") Long timId,
			@RequestParam (required=false, defaultValue = "0.0") Double poeniOd,
			@RequestParam (required=false, defaultValue = "") String pozicijaVozaca
			) throws IOException {
		//ovde preuzimamo listu parking karata ali iz baze, prvo se poziva servis, pa baza.
		List<Vozac> vozaci = new ArrayList<Vozac>();
		vozaci = VozacService.find(timId, poeniOd, pozicijaVozaca);
		//da li je potrebna lista zona:
		ModelAndView rezultat = new ModelAndView("zadatak7");

		rezultat.addObject("vozaci", vozaci);

		Vozac vozacSaNajmanjimBrojemPoena=vozaci.get(0);
		Vozac vozacSaNajvecimBrojemPoena=vozaci.get(0);

		for(Vozac v : vozaci) {
			if(v.getPoeni() < vozacSaNajmanjimBrojemPoena.getPoeni()) {
				vozacSaNajmanjimBrojemPoena = v;
			}
			if(v.getPoeni() > vozacSaNajvecimBrojemPoena.getPoeni()) {
				vozacSaNajvecimBrojemPoena = v;
			}
		}
		rezultat.addObject("vozacSaNajvecimBrojemPoena", vozacSaNajvecimBrojemPoena);
		rezultat.addObject("vozacSaNajmanjimBrojemPoena", vozacSaNajmanjimBrojemPoena);

		return rezultat;
	}


}
