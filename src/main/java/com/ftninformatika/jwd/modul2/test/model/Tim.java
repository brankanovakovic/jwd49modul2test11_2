package com.ftninformatika.jwd.modul2.test.model;

public class Tim {
	private Long id;
	private String naziv, sediste;
	
	public Tim() {
	}

	public Tim(Long id, String naziv, String sediste) {
		this.id = id;
		this.naziv = naziv;
		this.sediste = sediste;
	}
	
	public Tim(String naziv, String sediste) {
		this.naziv = naziv;
		this.sediste = sediste;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getSediste() {
		return sediste;
	}

	public void setSediste(String sediste) {
		this.sediste = sediste;
	}

	@Override
	public String toString() {
		return "Tim [id=" + id + ", naziv=" + naziv + ", sediste=" + sediste + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((sediste == null) ? 0 : sediste.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tim other = (Tim) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (sediste == null) {
			if (other.sediste != null)
				return false;
		} else if (!sediste.equals(other.sediste))
			return false;
		return true;
	}

	
	
	
}
