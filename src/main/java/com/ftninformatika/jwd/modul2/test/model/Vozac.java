package com.ftninformatika.jwd.modul2.test.model;

import java.time.LocalDate;

public class Vozac {
	
	private int id, poeni, brojVozaca;
	private Tim tim;
	private String imeIPrezime;
	private LocalDate datumRodjenja;
	private PozicijaVozaca pozicijaVozaca;
	
	public Vozac() {
	}

	public Vozac(int id, int poeni, int brojVozaca, Tim tim, String imeIPrezime, LocalDate datumRodjenja,
			PozicijaVozaca pozicijaVozaca) {
		this.id = id;
		this.poeni = poeni;
		this.brojVozaca = brojVozaca;
		this.tim = tim;
		this.imeIPrezime = imeIPrezime;
		this.datumRodjenja = datumRodjenja;
		this.pozicijaVozaca = pozicijaVozaca;
	}
	
	public Vozac(int poeni, int brojVozaca, Tim tim, String imeIPrezime, LocalDate datumRodjenja,
			PozicijaVozaca pozicijaVozaca) {
		this.poeni = poeni;
		this.brojVozaca = brojVozaca;
		this.tim = tim;
		this.imeIPrezime = imeIPrezime;
		this.datumRodjenja = datumRodjenja;
		this.pozicijaVozaca = pozicijaVozaca;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPoeni() {
		return poeni;
	}

	public void setPoeni(int poeni) {
		this.poeni = poeni;
	}

	public int getBrojVozaca() {
		return brojVozaca;
	}

	public void setBrojVozaca(int brojVozaca) {
		this.brojVozaca = brojVozaca;
	}

	public Tim getTim() {
		return tim;
	}

	public void setTim(Tim tim) {
		this.tim = tim;
	}

	public String getImeIPrezime() {
		return imeIPrezime;
	}

	public void setImeIPrezime(String imeIPrezime) {
		this.imeIPrezime = imeIPrezime;
	}

	public LocalDate getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public PozicijaVozaca getPozicijaVozaca() {
		return pozicijaVozaca;
	}

	public void setPozicijaVozaca(PozicijaVozaca pozicijaVozaca) {
		this.pozicijaVozaca = pozicijaVozaca;
	}

	@Override
	public String toString() {
		return "Vozac [id=" + id + ", poeni=" + poeni + ", brojVozaca=" + brojVozaca + ", tim=" + tim + ", imeIPrezime="
				+ imeIPrezime + ", datumRodjenja=" + datumRodjenja + ", pozicijaVozaca=" + pozicijaVozaca + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + brojVozaca;
		result = prime * result + ((datumRodjenja == null) ? 0 : datumRodjenja.hashCode());
		result = prime * result + id;
		result = prime * result + ((imeIPrezime == null) ? 0 : imeIPrezime.hashCode());
		result = prime * result + poeni;
		result = prime * result + ((pozicijaVozaca == null) ? 0 : pozicijaVozaca.hashCode());
		result = prime * result + ((tim == null) ? 0 : tim.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vozac other = (Vozac) obj;
		if (brojVozaca != other.brojVozaca)
			return false;
		if (datumRodjenja == null) {
			if (other.datumRodjenja != null)
				return false;
		} else if (!datumRodjenja.equals(other.datumRodjenja))
			return false;
		if (id != other.id)
			return false;
		if (imeIPrezime == null) {
			if (other.imeIPrezime != null)
				return false;
		} else if (!imeIPrezime.equals(other.imeIPrezime))
			return false;
		if (poeni != other.poeni)
			return false;
		if (pozicijaVozaca != other.pozicijaVozaca)
			return false;
		if (tim == null) {
			if (other.tim != null)
				return false;
		} else if (!tim.equals(other.tim))
			return false;
		return true;
	}
	
}
