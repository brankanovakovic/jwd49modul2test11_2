package com.ftninformatika.jwd.modul2.test.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul2.test.model.PozicijaVozaca;
import com.ftninformatika.jwd.modul2.test.model.Tim;
import com.ftninformatika.jwd.modul2.test.model.Vozac;
@Component
public class VozacDAO {


	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static class VozacRowMapper implements RowMapper<Vozac> {

		public Vozac mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long timId = rs.getLong(index++);
			String imeIPrezime = rs.getString(index++);
			Integer poeni = rs.getInt(index++);
			LocalDate datumRodjenja = rs.getDate(index++).toLocalDate();
			Integer brojVozaca = rs.getInt(index++);
			String pozicijaVozaca = rs.getString(index++);
			String naziv = rs.getString(index++);
			String sediste = rs.getString(index++);
			
			Tim tim = new Tim(timId, naziv, sediste);
			PozicijaVozaca search = null;
			for(PozicijaVozaca text : PozicijaVozaca.values()) {
				if(text.name().equals(pozicijaVozaca)) {
					search=text;
				}
			}
			
			Vozac vozac = new Vozac(poeni, brojVozaca, tim, imeIPrezime, datumRodjenja, search);
			return vozac;
		}
	}
	
	public List<Vozac> findAll() {
		String sql = 
				"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste  FROM vozaci v " +
				"LEFT JOIN timovi t ON v.timId = t.id " +
				"ORDER BY v.id";
		return jdbcTemplate.query(sql, new VozacRowMapper());
	}

	public Vozac findOne(String imeIPrezime) {
		String sql = 
				"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste FROM vozaci v " + 
				"LEFT JOIN timovi t ON v.timId = t.id " + 
				"WHERE v.imeIPrezime = ? " + 
				"ORDER BY v.id";
		return jdbcTemplate.queryForObject(sql, new VozacRowMapper(), imeIPrezime);
	}

	public void save(Vozac vozac) {
		String sql = "INSERT INTO vozaci (timId, imeIPrezime, poeni, datumRodjenja, brojVozaca, pozicijaVozaca) VALUES (?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql,  vozac.getTim().getId(), vozac.getImeIPrezime(), vozac.getPoeni(), vozac.getDatumRodjenja(), vozac.getBrojVozaca(), vozac.getPozicijaVozaca());
	}

	public void delete(String imeIPrezime) {
		String sql = "DELETE FROM vozaci WHERE imeIPrezime = ?";
		jdbcTemplate.update(sql, imeIPrezime);
	}

	public List<Vozac> find(Long timId, Double poeniOd, String pozicijaVozaca) {
	//	pozicijaVozaca = (pozicijaVozaca.equals(""))? "%" + pozicijaVozaca + "%" : pozicijaVozaca;
		if(poeniOd == 0.0 && pozicijaVozaca.length() == 0 && timId != 0) {
			String sql = 
					"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste FROM vozaci v " + 
					"LEFT JOIN timovi t ON v.timId = t.id " + 
					"WHERE v.timId = ? " + 
					"ORDER BY v.id";
			return jdbcTemplate.query(sql, new VozacRowMapper(), timId);
		}else if(poeniOd == 0 && timId != 0 && pozicijaVozaca.length() != 0) {
			String sql = 
					"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste FROM vozaci v " + 
					"LEFT JOIN timovi t ON v.timId = t.id " + 
					"WHERE v.timId = ? AND v.pozicijaVozaca LIKE ? " + 
					"ORDER BY v.id";
			return jdbcTemplate.query(sql, new VozacRowMapper(), timId, pozicijaVozaca);
		}else if(pozicijaVozaca.length() == 0 && timId != 0 && poeniOd != 0) {
			String sql = 
					"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste FROM vozaci v " + 
					"LEFT JOIN timovi t ON v.timId = t.id " + 
					"WHERE v.timId = ? AND v.poeni >= ? " + 
					"ORDER BY v.id";
			return jdbcTemplate.query(sql, new VozacRowMapper(), timId, poeniOd);
		}else if(pozicijaVozaca.length() != 0 && poeniOd != 0 && timId !=0) {
			String sql = 
					"SELECT v.timId, v.imeIPrezime, v.poeni, v.datumRodjenja, v.brojVozaca, v.pozicijaVozaca, t.naziv, t.sediste FROM vozaci v " + 
					"LEFT JOIN timovi t ON v.timId = t.id " + 
					"WHERE v.timId = ? AND v.poeni >= ? AND v.pozicijaVozaca LIKE ? " + 
					"ORDER BY v.id";
			return jdbcTemplate.query(sql, new VozacRowMapper(), timId, poeniOd, pozicijaVozaca);
		}else {
			return findAll();
		}
	}
	
}
