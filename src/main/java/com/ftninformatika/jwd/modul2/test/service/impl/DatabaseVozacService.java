package com.ftninformatika.jwd.modul2.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.dao.VozacDAO;
import com.ftninformatika.jwd.modul2.test.model.Vozac;
import com.ftninformatika.jwd.modul2.test.service.VozacService;

@Service
public class DatabaseVozacService implements VozacService{

	@Autowired
	VozacDAO vozacDao;
	
	public List<Vozac> findAll() {
		return vozacDao.findAll();
	}

	public Vozac findOne(String imeIPrezime) {
		return vozacDao.findOne(imeIPrezime);
	}

	public Vozac update(Vozac vozac) {
		
		vozacDao.save(vozac);
		return vozac;
	}

	public void delete(String imeIPrezime) {
		vozacDao.delete(imeIPrezime);
	}

	public List<Vozac> find(Long timId, Double poeniOd, String pozicijaVozaca) {
		if(timId == null) {
			timId = 0L;
		}
		if (poeniOd == null) {
			poeniOd = 0.0;
		}
		if (pozicijaVozaca == null) {
			pozicijaVozaca = "";
		}
		
		return vozacDao.find(timId, poeniOd, pozicijaVozaca);
	}	
}
