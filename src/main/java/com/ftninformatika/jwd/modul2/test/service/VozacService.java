package com.ftninformatika.jwd.modul2.test.service;

import java.util.List;

import com.ftninformatika.jwd.modul2.test.model.Vozac;

public interface VozacService {
	//List<Vozac> find(Integer trajanjeOD, Integer trajanjeDO);
	List<Vozac> findAll();
	Vozac findOne(String imeIPrezime);
	Vozac update(Vozac vozac);
	void delete(String imeIPrezime);
	List<Vozac> find(Long timId, Double poeniOd, String pozicijaVozaca);
}
